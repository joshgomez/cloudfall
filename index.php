<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<title>CloudFall</title>

		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<meta name="keywords" content="Cloudfall,Game,Canvas,Xuniverse,Josh,JavasScript" />
		<meta name="description" content="A game made in canvas with JavaScript." />
		
		<meta name="author" content="Josh" />
		<meta name="generator" content="HTML,JavasScript" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		
		<link rel="stylesheet" href="style.css"/>
		
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<script>
		
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'GA_ID', 'YOUR-DOMAIN');
			ga('send', 'pageview');
		  
		</script>
		
	</head>
	<body>
	<header id='header'>
		<h1>Cloudfall</h1>
		<p>A project where I am experimenting with canvas. Hopefully later to be a real game.</p>
	</header>
		<canvas id="canvasGame" width="1280" height="600"></canvas>
		<footer id='footer'>
			2014 Josh 
			<a href='http://www.xuniver.se/'>Xuniverse</a> - 
			<a href='http://www.xuniver.se/page/projects#cloudfall'>Source coming later</a>
		</footer>
		
		<!--
		
			<script src="javascripts/game.resource.js"></script>
			<script src="javascripts/game.sprite.js"></script>
			<script src="javascripts/game.model.js"></script>
			<script src="javascripts/game.entity.js"></script>
			<script src="javascripts/game.rect.js"></script>
			<script src="javascripts/game.player.js"></script>
			<script src="javascripts/game.main.js"></script>
		
		-->
		
		<script src="javascripts.php"></script>
		<script>
		
			var game = new Game("canvasGame");
		
		</script>
	</body>
</html>