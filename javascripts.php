<?php

	define('IN_SITE',true);
	
	require __DIR__ . '/includes/functions.php';

	$offset = 3600 * 0;
	$etag = hash_file('md5',$_SERVER['SCRIPT_FILENAME']);

	ob_start('ob_gzhandler');
	
	setCacheHeader($etag,$offset);
	header('Content-Type: application/x-javascript; charset=utf-8');

	require __DIR__ . '/javascripts/game.resource.js';
	require __DIR__ . '/javascripts/game.sprite.js';
	require __DIR__ . '/javascripts/game.model.js';
	require __DIR__ . '/javascripts/game.entity.js';
	require __DIR__ . '/javascripts/game.rect.js';
	require __DIR__ . '/javascripts/game.player.js';
	require __DIR__ . '/javascripts/game.main.js';
	
	ob_end_flush();

?>