<?php

	function ifSetOr(&$var,$default = null){
	
		$temp = isset($var) ? $var : $default;
		return $temp;
	}
	
	function setCacheHeader($etag,$offset)
	{
		$time = time();
		
		header("Etag: ".$etag);
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s',$time) . ' GMT');
		header('Expires: ' . gmdate('D, d M Y H:i:s', $time + $offset) . ' GMT');
		header('Pragma: cache');
		header('Cache-Control: max-age=' . $offset . ', must-revalidate');
		header('Vary: Accept-Encoding');
	}
	
	