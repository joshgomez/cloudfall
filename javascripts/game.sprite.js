

function Sprite(image,width,height,x,y,cWidth,cHeight)
{
	this.image = image;
	this.width = width;
	this.height = height;
	
	var cWidth 	= (cWidth) 	? 	cWidth 	: [0,0];
	var cHeight = (cHeight) ?	cHeight : [0,0];
	
	this.cropWidth = cWidth;
	this.cropHeight = cHeight;
	
	this.pos = [x,y];
	
	this.canvas;
	this.ctx;
	this.pixelMap = [];
}

Sprite.prototype = {

	buildPixelMap:function()
	{
		var resolution = 1;

		var canvas = this.setCanvas();
		
		for( var y = 0; y < canvas.height; y++)
		{
			for( var x = 0; x < canvas.width; x++ )
			{
				var dataRowColOffset = y+"_"+x;
				
				var pixel = this.ctx.getImageData(x,y,resolution,resolution);
				var pixelData = pixel.data;
		 
				this.pixelMap[dataRowColOffset] = { x:x, y:y, pixelData: pixelData };
			}
		}
	},
	
	getSize:function()
	{
	
		var left 	= this.cropWidth[0];
		var right 	= this.cropWidth[1];
		
		var top 	= this.cropHeight[0];
		var bottom 	= this.cropHeight[1];
	
		var cropWidth 	= Math.floor(this.width*this.pos[0] + left);
		var totalWidth 	= this.width - (left + right);
		
		var cropHeight 		= Math.floor(this.height*this.pos[1] + top);
		var totalHeight 	= this.height - (top + bottom);
		
		return {cropWidth:cropWidth,totalWidth:totalWidth,cropHeight:cropHeight,totalHeight:totalHeight};
	},
	
	_drawImage:function(ctx,x,y)
	{
		var size = this.getSize();
	
		return ctx.drawImage(
			
			this.image, 
			size.cropWidth,size.cropHeight, 
			size.totalWidth,size.totalHeight, 
			x, y, 
			size.totalWidth, size.totalHeight
		);
	},
	
	_updateSource:function(source)
	{
		source.width = this.width - (this.cropWidth[0] + this.cropWidth[1]);
		source.height = this.height - (this.cropHeight[0] + this.cropHeight[1]);
	},
	
	setCanvas:function()
	{
		var size = this.getSize();
		
		this.canvas = document.createElement('canvas');
		
		this.canvas.width 	= size.totalWidth;
		this.canvas.height 	= size.totalHeight;
		
		this.ctx = this.canvas.getContext("2d");
		
		this.ctx.drawImage(
					
					this.image, 
					size.cropWidth,size.cropHeight, 
					size.totalWidth,size.totalHeight, 
					0, 0, 
					size.totalWidth, size.totalHeight
		);
		
		return {self:this.canvas,width:size.totalWidth,height:size.totalHeight}
	},
	
	getPattern:function(ctx,repeat)
	{
		var repeat = repeat || "repeat";
		return ctx.createPattern(this.canvas,repeat);
	}
}

