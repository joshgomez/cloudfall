
function Model(game,source)
{
	this.game 	= game;
	this.source = source;
	
	this.framesIdle = [];
	this.framesJump = [];
	this.framesFall = [];
	this.framesMove = [];
	
	this.frameInterval 		= 0;
	this.frameIndex 		= 0;
	
	this.width 		= 0;
	this.height 	= 0;
	
	this.minWidth 	= 0;
	this.minHeight 	= 0;
	
	this.maxWidth 	= 0;
	this.maxHeight 	= 0;
}

Model.prototype = {

	_draw:function()
	{
        try
		{
			var frames;
			if(this.source.isJumping || this.source.isFalling)
			{
				frames = this.framesJump[this.frameIndex];
			}
			else if(this.source.isMoving)
			{
				frames = this.framesMove[this.frameIndex];
			}
			else frames = this.framesIdle[this.frameIndex];
			
			if(!frames)
				frames = this.framesIdle[0];
			
			frames._updateSource(this.source);
			frames._drawImage(this.game._ctx,this.source.pos[0],this.source.pos[1]);
        }
		catch (e) {console.log(e)}
	},
	
	resetJump:function()
	{
		if(this.source.isMovingLeft) this.frameIndex = 0;
		else this.frameIndex = Math.floor(this.framesJump.length)/2;
	},
	
	resetMoveLeft:function()
	{
		if(!this.source.isMoving || !this.source.isMovingLeft)
		{
			this.frameIndex = 0;
		}
	},
	
	resetMoveRight:function()
	{
		if(!this.source.isMoving || this.source.isMovingLeft)
		{
			this.frameIndex = Math.floor(this.framesMove.length)/2;
		}
	},
	
	resetFall:function()
	{
		if(this.source.isFalling)
		{
			if(this.source.isMovingLeft) this.frameIndex = 0;
			else this.frameIndex = Math.floor(this.framesIdle.length)/2;
		}
	}
}