
function ModelPlayer(game,source)
{
	Model.call(this,game,source);
	
	var sprites = game.resource.get('images/hero_sprites.png');
	var sprites2 = game.resource.get('images/hero_sprites2.png');
	
	var cropIdleWidth = [34,34];
	var cropJumpWidth = [34,34];
	var cropMoveWidth = [14,14];
	
	this.width = 144;
	this.height = 144;
	
	this.minWidth 	= this.width - (cropIdleWidth[0] + cropIdleWidth[1]);
	this.minHeight 	= this.height;
	
	this.maxWidth 	= this.width - (cropMoveWidth[0] + cropMoveWidth[1]);
	this.maxHeight 	= this.height;
	
	this.framesIdle.push(new Sprite(sprites,this.width,this.height,0,0,cropIdleWidth));
	
	this.framesIdle.push(new Sprite(sprites,this.width,this.height,1,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites,this.width,this.height,2,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites,this.width,this.height,3,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites,this.width,this.height,4,0,cropIdleWidth));
	
	this.framesIdle.push(new Sprite(sprites2,this.width,this.height,5,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites2,this.width,this.height,4,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites2,this.width,this.height,3,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites2,this.width,this.height,2,0,cropIdleWidth));
	this.framesIdle.push(new Sprite(sprites2,this.width,this.height,1,0,cropIdleWidth));
	
	//Jumps
	this.framesJump.push(new Sprite(sprites,this.width,this.height,4,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites,this.width,this.height,3,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites,this.width,this.height,2,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites,this.width,this.height,1,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites,this.width,this.height,0,1,cropJumpWidth));
	
	this.framesJump.push(new Sprite(sprites2,this.width,this.height,1,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites2,this.width,this.height,2,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites2,this.width,this.height,3,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites2,this.width,this.height,4,1,cropJumpWidth));
	this.framesJump.push(new Sprite(sprites2,this.width,this.height,5,1,cropJumpWidth));
	
	
	//Moves
	this.framesMove.push(new Sprite(sprites,this.width,this.height,0,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites,this.width,this.height,1,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites,this.width,this.height,2,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites,this.width,this.height,3,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites,this.width,this.height,4,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites,this.width,this.height,5,2,cropMoveWidth));
	
	this.framesMove.push(new Sprite(sprites2,this.width,this.height,5,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites2,this.width,this.height,4,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites2,this.width,this.height,3,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites2,this.width,this.height,2,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites2,this.width,this.height,1,2,cropMoveWidth));
	this.framesMove.push(new Sprite(sprites2,this.width,this.height,0,2,cropMoveWidth));
}

ModelPlayer.prototype = Object.create(Model.prototype);
ModelPlayer.prototype.constructor = ModelPlayer;

ModelPlayer.prototype._updateFrames = function(){

	if(this.source.isJumping)
	{
		var jumpLength = this.source.speedJump/this.source.maxSpeedJump;
		if(this.source.isMovingLeft)
		{
			if(jumpLength >= 0.80) this.frameIndex = 0;
			else if(jumpLength >= 0.60) this.frameIndex = 1;
			else this.frameIndex = 2;
		}
		else
		{
			if(jumpLength >= 0.80) this.frameIndex = 5;
			else if(jumpLength >= 0.60) this.frameIndex = 6;
			else this.frameIndex = 7;
		}
	}
	else if(this.source.isFalling)
	{
		var fallLength = this.source.speedFall/this.source.maxSpeedJump;
		if(this.source.isMovingLeft)
		{
			if(fallLength > 0.90) this.frameIndex = 4;
			else this.frameIndex = 2;
		}
		else
		{
			if(fallLength > 0.90) this.frameIndex = 9;
			else this.frameIndex = 7;
		}
	}
	else if(this.source.isMoving)
	{
		if(this.frameInterval >= 60)
		{
			if(this.source.isMovingLeft)
			{
				if (this.frameIndex >= Math.floor(this.framesMove.length)/2 - 1) this.frameIndex = 0;
				else this.frameIndex++;
			}
			else
			{
				if (this.frameIndex >= this.framesMove.length-1) this.frameIndex = Math.floor(this.framesMove.length)/2;
				else this.frameIndex++;
			}
			
			this.frameInterval = 0;
		}
		
		var speed = Math.abs(this.source.speedMove)+this.source.maxSpeedMove;
		if(!isNaN(speed))
			this.frameInterval += speed;
	}
	else
	{
		if(this.frameInterval >= 60)
		{
			if(this.source.isMovingLeft)
			{
				if (this.frameIndex >= Math.floor(this.framesIdle.length)/2 - 1) this.frameIndex = 0;
				else this.frameIndex++;
			}
			else
			{
				if (this.frameIndex >= this.framesIdle.length-1) this.frameIndex = Math.floor(this.framesIdle.length)/2;
				else this.frameIndex++;
			}
			
			this.frameInterval = 0;
		}
		
		var speed = this.source.maxSpeedIdle*this.game._dt + this.source.maxSpeedIdle;
		if(!isNaN(speed))
			this.frameInterval += speed;
	}
}

function Player(game)
{
	Entity.call(this,game);
	this.model = new ModelPlayer(game,this);
}

Player.prototype = Object.create(Entity.prototype);
Player.prototype.constructor = Player;