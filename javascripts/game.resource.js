
function Resource()
{
    this.resourceCache = {};
    this.loading = [];
    this.readyCallbacks = [];
}

Resource.prototype = {

	load:function(urlArray){
	
		var self = this;
        if(urlArray instanceof Array){
		
            urlArray.forEach(function(url){
			
                self._load(url);
            });
        }
        else
		{
            this._load(urlArray);
        }
	},
	
	_load:function(url){
	
        if(this.resourceCache[url]){
		
            return this.resourceCache[url];
        }
        else
		{
			var self = this;
            var img = new Image();
            img.onload = function(){
			
                self.resourceCache[url] = img;
                if(self.isReady()){
				
                    self.readyCallbacks.forEach(function(func) { func(); });
                }
            };
			
            this.resourceCache[url] = false;
            img.src = url;
        }
	},
	
	get:function(url){
	
		return this.resourceCache[url];
	},
	
    isReady:function(){
	
        var ready = true;
        for(var k in this.resourceCache)
		{
            if(this.resourceCache.hasOwnProperty(k) && !this.resourceCache[k])
                ready = false;
        }
		
        return ready;
    },
	
    onReady:function(func)
	{
        this.readyCallbacks.push(func);
    }
}