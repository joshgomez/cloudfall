
window.requestAnimFrame = (function()
{
	return  window.requestAnimationFrame 	||
		window.webkitRequestAnimationFrame 	||
		window.mozRequestAnimationFrame    	||
		function( callback ){ window.setTimeout(callback, 1000 / 60); };
	
})();

function degToRad(degrees)
{
	return degrees * Math.PI / 180;
}

function Circle(game,radius,color)
{
	var radius = radius || 1;
	var color = color || [255,255,255,1];

	this.game = game;
	this.color = color;
	this.radius = radius;
	this.pos = [0,0];
}

Circle.prototype = {

	setPos:function(x,y)
	{
		this.pos = [x,y];
	},
	
	_update:function(deltaX,deltaY)
	{
		if (
			(this.pos[1] - this.radius) > this.game._height || 
			(this.pos[0] - this.radius) > this.game._width	|| 
			(this.pos[0] + this.radius) < 0
		){

			this.pos[0] 		= Math.round(Math.random() * this.game._width);
			this.radius 		= Math.round(Math.random() * 100);
			this.pos[1] 		= 0 - this.radius;
			this.color[3] 		= Math.random() / 2;
			

		}
		else
		{
			this.pos[0] -= deltaX;
			this.pos[1] += Math.round(deltaY * this.game._dt);
		}
	},

	_draw:function()
	{
		this.game._ctx.save();
		this.game._ctx.beginPath();
		this.game._ctx.fillStyle = 'rgba(' + this.color.join() + ')';
		this.game._ctx.arc(this.pos[0], this.pos[1], this.radius, degToRad(0), degToRad(360), true);
		this.game._ctx.closePath();
		this.game._ctx.fill();
		this.game._ctx.restore();
	}
}

function Game(el)
{
	this.resource = new Resource();
	
	this.resource.load([
		'images/hero_sprites.png',
		'images/hero_sprites2.png',
		'images/hero_sprites3.png',
		'images/hero_sprites4.png',
		'images/sprites32x32.png',
		'images/sprites256x256.png',
	]);
	
	this.resource.onReady(this._init.bind(this));

	this._el = document.getElementById(el);
	this._width = 0;
	this._height = 0;
	
	this._active = 1;
	
	this._keymap = [];
	this._lastKey = 0;
	
	this._lastTime = Date.now();
	this._fps = 0;
	this._dt = 0;
	
	this._ctx = null;
	
	this.player = {};
	this.circles = [];
	this.platforms = [];
	
	this.backgroundObjects = []
	this.objects = [];
}

Game.prototype = {

	_init:function(){
	
		if(this._el)
		{
			this._width 		= this._el.width;
			this._height 		= this._el.height;
			
			this._ctx = this._el.getContext("2d");
			this._clear();
			
			this.player = new Player(this);
			
			this._tick();
			this._initChanges();
		}
	},
	
	_initChanges:function(){

		this.player.setPos(
		
			Math.floor((this._width - this.player.width)/2),  
			Math.floor((this._height - this.player.height)/2)
		);

		
		this.player.isFalling = true;
		
		for (var i = 0; i < 35; i++)
		{
			this.addCircles(
			
				Math.round(Math.random() * this._width),
				Math.round(Math.random() * this._height),
				Math.random() * 80,
				[255,255,255,Math.random() / 2]
				
			);
		}
		
		
		
		
		var ground = this.addPlatform(-(this._width*2),this._height-32,this._width*5,32);
		ground.setPattern(this.resource.get('images/sprites32x32.png'),32,32,0,0);
		
		var grass = new Rect(this,96,32);
		var tree = new Rect(this,256,256);
		
		
		grass.setPos(64,this._height-64);
		grass.setPattern(this.resource.get('images/sprites32x32.png'),32,32,1,0);
		
		tree.setPos(600,this._height-(256+32));
		tree.setPattern(this.resource.get('images/sprites256x256.png'),256,256,0,0);
		
		this.objects.push(grass);
		this.backgroundObjects.push(tree);
		
		this.addPlatform(400,450,70,10);
		var test = this.addPlatform(200,350,70,10);
		test.moving = true;
		
		this._event();
	},
	
	drawText:function(msg,x,y,font,color){
	
		var font = font || "12px Consolas";
		var color = color || [255,255,255,1];
	
		this._ctx.save();
		
		this._ctx.font = font;
		this._ctx.fillStyle = 'rgba(' + color.join() + ')';
		this._ctx.fillText(msg,x,y);
		
		this._ctx.restore(); 
	},
	
	addPlatform:function(x,y,width,height)
	{
		var platform = new Rect(this,width,height);
		platform.setPos(x,y);
		
		this.platforms.push(platform);
		
		return platform;
	},
	
	addCircles:function(x,y,radius,color)
	{
		var circle = new Circle(this,radius,color);
		circle.setPos(x,y);
		this.circles.push(circle);
		
		return circle;
	},
	
	_drawObjects:function(infront)
	{
		if(!infront)
		{
			for (var i = 0; i < this.circles.length; i++)
			{
				this.circles[i]._draw();
			}
			
			for (var i = 0; i < this.platforms.length; i++)
			{
				this.platforms[i]._draw();
			}
			
			for (var i = 0; i < this.backgroundObjects.length; i++)
			{
				this.backgroundObjects[0]._draw();
			}
			
			return;
		}
		
		for (var i = 0; i < this.objects.length; i++)
		{
			this.objects[0]._draw();
		}
	},
	
	_clear:function()
	{
			this._ctx.clearRect(0, 0, this._width,this._height);
			
			this._ctx.fillStyle = "rgba(178, 48, 116, 1)";
			this._ctx.beginPath();
			this._ctx.rect(0, 0, this._width,this._height);
			this._ctx.closePath();
			this._ctx.fill();
	},
	
	_checkCollision:function()
	{
		var self = this;
		this.platforms.forEach(function(platform, index)
		{
			if((self.player.onPlatform == -1 || self.player.isFalling) && self.player._checkCollision(platform))
			{
				self.player.onPlatform = index;
			}
		})
	},
	
	_tick:function()
	{
		var now = Date.now();
		
		if(this.lastTime != 0 && this._active){
		
			this._dt = (now - this.lastTime) / 1000.0;
			if(this._dt)
			{
				this._clear();
				this._animate()
				
				this._checkCollision();
				
				this._draw();
				
				var timeFrame = 1000/(now - this.lastTime) ;
				this._fps += (timeFrame - this._fps)/60;
				
				this.drawText('FPS: ' + this._fps.toFixed(1),4,12);
			}
		}
		
		this.lastTime = now;
		window.requestAnimFrame(this._tick.bind(this));
	},
	
	_animate:function()
	{		
		if(this._keymap[87])
			this.player.jump();

		if(this._keymap[65] && !this._keymap[68])
		{
			this.player.moveLeft();
		}
		
		if(this._keymap[68] && !this._keymap[65])
		{
			this.player.moveRight();
		}
		
		this.player._update();
		
		var moveSpeed = 0;
		if(this.player._isFar()){
		
			moveSpeed = this.player.speedMove;
		}
		
		for (var i = 0; i < this.circles.length; i++)
		{
			this.circles[i]._update(moveSpeed/3,50);
		}
		
		for (var i = 0; i < this.objects.length; i++)
		{
			var x = this.objects[i].pos[0];
			var y = this.objects[i].pos[1];
			
			this.objects[i].setPos(x-moveSpeed,y);
		}
	
		for (var i = 0; i < this.backgroundObjects.length; i++)
		{
			var x = this.backgroundObjects[i].pos[0];
			var y = this.backgroundObjects[i].pos[1];
			
			this.backgroundObjects[i].setPos(x-moveSpeed,y);
		}
		
		for (var i = 0; i < this.platforms.length; i++)
		{
			var x = this.platforms[i].pos[0];
			var y = this.platforms[i].pos[1];
			
			if(this.platforms[i].moving)
			{
				if((this.platforms[i].mapPos[0]) < 250){
					this.platforms[i].mapPos[0] += 1;
					x += 1;
				}
			}
			
			
			
			this.platforms[i].setPos(x-moveSpeed,y);
		}
	},
	
	_draw:function()
	{
		this._drawObjects();
		this.player._draw();
		this._drawObjects(true);
	},
	
	_event:function()
	{
		var self = this;
		var keypress = function(e){
			e = e || event;
			self._keymap[e.keyCode] = e.type == 'keydown';
		}
	
		window.addEventListener("keypress",keypress);
		window.addEventListener("keydown",keypress);
		window.addEventListener("keyup",keypress);
		
		window.addEventListener("blur",function(){
		
			self._keymap = [];
			self._active = 0;
		
		});
		
		window.addEventListener("focus",function(){
		
			self._keymap = [];
			self._active = 1;
		
		});
	}
}