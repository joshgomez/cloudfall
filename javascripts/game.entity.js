
function Entity(game)
{
	this.game = game;
	this.model;
	
	this.maxSpeedIdle = 4;
	this.maxSpeedMove = 5;
	this.maxSpeedJump = 17;

	this.isIdle 	= true;
	this.isJumping 	= false;
	this.isFalling 	= false;
	
	this.onPlatform 	= -1;
	
	this.isMoving 		= false;
	this.isMovingLeft 	= true;
	
	this.speedJump = 0;
	this.speedFall = 0;
	this.speedMove = 0;
	
	this.width 	= 0;
	this.height = 0; 
	
	this.pos 	= [0,0];
	this.mapPos = null;
}

Entity.prototype = {

	setPos:function(x,y)
	{
		var x = Math.floor(x);
		var y = Math.floor(y);
	
		if(!this._isFar())
		{
			this.pos[0] = x;
		}
		
		this.pos[1] = y;
		
		if(!this.mapPos){
		
			this.mapPos = [x,y];
		}
		else
		{
			this.mapPos[0] += this.speedMove;
			this.mapPos[1] = y;
		}
	},
	
	jump:function()
	{
		if (!this.isJumping && !this.isFalling)
		{
			this.model.resetJump();
			
			this.onPlatform = -1;
			this.speedFall = 0;
			this.isJumping = true;
			this.speedJump = this.maxSpeedJump;
		}
	},
	
	_getJumpSpeed:function()
	{
		var extraSpeed = 0;
		if(this.isJumping)
			extraSpeed = Math.floor(this.speedJump/10);
			
		return extraSpeed;
	},
	
	moveLeft:function()
	{
		this.model.resetMoveLeft();
		
		if(this.pos[0] > 0)
		{
			this.isMovingLeft = true;
			this.speedMove = -(this.maxSpeedMove) - this._getJumpSpeed();
			this.isMoving = true;
		}
	},
	
	moveRight:function()
	{
		this.model.resetMoveRight();

		if(this.pos[0] + this.width < game._width)
		{
			this.isMovingLeft = false;
			this.speedMove = this.maxSpeedMove + this._getJumpSpeed();
			this.isMoving = true;
		}
	},
	
	_checkJump:function()
	{
		if (this.speedJump < 1)
		{
			this.isJumping = false;
			this.isFalling = true;
			this.speedFall = 1;
		}
		else
		{
			this.speedJump -= 50*this.game._dt;
			this.setPos(this.pos[0], this.pos[1] - this.speedJump);
		}
	},
	
	_checkMove:function()
	{
		var newX = this.pos[0] + this.speedMove;
		if(newX+this.model.minWidth > game._width)
		{
			this.isMoving = false;
			this.speedMove = 0;
			this.setPos( game._width - this.model.minWidth, this.pos[1]);
			return;
		}
		
		if(newX < 0)
		{
			this.isMoving = false;
			this.speedMove = 0;
			this.setPos(0, this.pos[1]);
			return;
		}
		
		if(this.onPlatform != -1 && !this.isFalling)
		{
			var falldown = false
			var platform = this.game.platforms[this.onPlatform];
			if((newX > platform.pos[0]+platform.width))
			{
				this.setPos((platform.pos[0]+platform.width+1+(this.model.maxWidth-this.model.minWidth)), this.pos[1]);
				falldown = true;
			}
			else if(newX+this.width < platform.pos[0])
			{
				this.setPos((platform.pos[0]-this.model.maxWidth)-1, this.pos[1]);
				falldown = true;
			}
			
			if(falldown)
			{
				this.onPlatform = -1;
				this.isMoving = false;
				this.isJumping=false;
				this.isFalling=true;
			}
		}
		
		if(this.isMoving)
		{
			var speed =  this.maxSpeedMove*this.game._dt;
		
			if(this.isMovingLeft) this.speedMove += speed*10;
			else this.speedMove -= speed*10;
		
			if((this.isMovingLeft && this.speedMove > -1) || (this.speedMove < 1 && !this.isMovingLeft))
			{
				this.isMoving = false;
				this.speedMove = 0;
				
				newX = this.pos[0];
			}
			
			this.setPos(newX, this.pos[1]);
		}
		else this.speedMove = 0;
	},
	
	_isFar:function()
	{
		if((this.pos[0]+this.width > this.game._width*0.85 && this.speedMove > 0) || 
		(this.pos[0] < this.game._width*0.15 && this.speedMove < 0))
		{
			return true;
		}
		
		return false;
	},
	
	_isAbove:function()
	{
		if(this.pos[1] < this.game._height*0.40 && this.speedJump > 0)
		{
			return true;
		}
		
		return false;
	},
	
	_checkFall:function()
	{
		this.onPlatform = -1;
		if (this.pos[1]+this.height < game._height)
		{
			this.speedFall += 50*this.game._dt;
			this.setPos(this.pos[0], this.pos[1] + this.speedFall);
		}
		else
		{
			this.fallStop();
		}
	},
	
	_checkHitbox:function(target)
	{
		var sourceWidth 	= this.pos[0] + this.width;
		var sourceHeight 	= this.pos[1] + this.height;

		var targetWidth 	= target.pos[0] 	+ target.width;
		var targetHeight 	= target.pos[1]  	+ target.height;
		
		if(
			(this.pos[0] < targetWidth) && 
			(sourceWidth > target.pos[0]) && 
			(sourceHeight > target.pos[1] ) && 
			(sourceHeight < targetHeight)) return true;
				
		return false;
	},
	
	_checkCollision:function(target)
	{
		if(this._checkHitbox(target))
		{
			this.fallStop();
			if(this.pos[1] + this.height > target.pos[1])
			{
				var y = target.pos[1] - this.height;
				y += 1;
				
				this.setPos(this.pos[0],y);
			}
			
			return true;
		}
		
		return false;
	},
	
	fallStop:function()
	{
		this.model.resetFall();
		
		this.isFalling = false;
		this.speedFall = 0; 
		
		var y = this.pos[1];
		if(this.pos[1]+this.height > game._height)
			y = game._height-this.height;
		
		y += 1;
		this.setPos(this.pos[0],y);
	},
	
	_update:function()
	{
		if(this.isJumping || this.isFalling || this.isMoving)
		{
			this.isIdle = false;
		
			if (this.isJumping) this._checkJump();
			if (this.isFalling) this._checkFall();
		}
		else
		{
			this.isIdle = true;
		}
		
		this._checkMove();
		this.model._updateFrames();
	},
	
	_draw:function()
	{
		this.model._draw();
	}
}

