
function Rect(game,width,height,x,y)
{
	this.game = game;
	
	this.sprite;
	
	var x = x ? x : 0;
	var y = y ? y : 0;

	this.width 		= width;
	this.height 	= height;
	
	this.color = [255,255,255,1.0];
	
	this.pos = [x,y];
	this.mapPos = null;
	
	this.moving = false;
}

Rect.prototype = {

	setPos:function(x,y)
	{
		this.pos = [x,y];
		
		if(!this.mapPos)
			this.mapPos = [x,y];
	},
	
	setPattern:function(image,width,height,x,y,cWidth,cHeight)
	{
		this.sprite = new Sprite(image,width,height,x,y,cWidth,cHeight);
		this.sprite.setCanvas();
	},
	
	_draw:function()
	{
		this.game._ctx.fillStyle = 'rgba(255, 255, 255, 1)';
		
		if(this.sprite)
		{
			this.game._ctx.save(); 
			
			ptn = this.sprite.getPattern(this.game._ctx,'repeat');
			
			this.game._ctx.translate(this.pos[0] - this.width,this.pos[1] - this.height);
			
			this.game._ctx.fillStyle = ptn;
			this.game._ctx.fillRect(this.width,  this.height, this.width, this.height);
			
			this.game._ctx.restore();
		}
		else
		{
			this.game._ctx.save();
			
			this.game._ctx.fillStyle = 'rgba(' + this.color.join() + ')';
			this.game._ctx.fillRect(this.pos[0], this.pos[1], this.width, this.height);
			
			this.game._ctx.restore();
		}
	}
}

